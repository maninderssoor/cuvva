import Foundation

class MockNetworkEmptyResponse: NetworkManager {

    override func fetch<E, P, R>(with type: E.Type, and parameters: P, and response: R.Type, completion: @escaping ((APIProtocolCompletion) -> Void)) where E : APIProtocol, P : APIProtocolParameters, R : APIProtocolResponse {
        let protocolCompletion = APIProtocolCompletion(isSuccess: false,
                                                       service: type)
        completion(protocolCompletion)
    }
}
