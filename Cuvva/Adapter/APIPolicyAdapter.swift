import Foundation

class APIPolicyAdapter {
    
    func toPolicy(with apiPolicy: APIPolicy) -> [String: Any]? {
        guard let createdDate = apiPolicy.timestamp.dateFromString(),
            let newPolicy = apiPolicy.policyPayload,
            let startTime = newPolicy.start_date.dateFromString(),
            let endTime = newPolicy.end_date.dateFromString() else { return nil }
        
        print("Policy start time \(startTime), end time \(endTime)")
        return [PolicyKeys.uuid.rawValue: apiPolicy.uuid,
                PolicyKeys.id.rawValue: newPolicy.policy_id,
                PolicyKeys.originalId.rawValue: newPolicy.original_policy_id,
                PolicyKeys.createdDateTime.rawValue: createdDate,
                PolicyKeys.startDateTime.rawValue: startTime,
                PolicyKeys.endDateTime.rawValue: endTime,
                PolicyKeys.telephone.rawValue: newPolicy.incident_phone,
                PolicyKeys.referenceCode.rawValue: newPolicy.reference_code]
    }
    
    func toVehicle(with apiPolicy: APIPolicy) -> [String: Any]? {
        guard let policy = apiPolicy.policyPayload else { return nil }
        
        return [VehicleKeys.vrm.rawValue: policy.vrm,
                VehicleKeys.prettyVrm.rawValue: policy.prettyVrm,
                VehicleKeys.make.rawValue: policy.make,
                VehicleKeys.model.rawValue: policy.model,
                VehicleKeys.color.rawValue: policy.color]
    }
    
    func toTransaction(with policy: APIPolicy) -> [String: Any]? {
        guard let transaction = policy.transactionPayload,
            let createdDateTime = policy.timestamp.dateFromString() else { return nil }
        
        return [TransactionKeys.adminFee.rawValue: Decimal(transaction.extra_fees),
                TransactionKeys.uuid.rawValue: policy.uuid,
                TransactionKeys.policyId.rawValue: transaction.policy_id,
                TransactionKeys.premium.rawValue: Decimal(transaction.total_premium),
                TransactionKeys.premiumTax.rawValue: Decimal(transaction.ipt),
                TransactionKeys.createdDateTime.rawValue: createdDateTime]
    }
}
