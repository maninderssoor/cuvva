import Foundation
import UIKit

struct Button {
    
    static var `default`: Style {
        return Style(alignment: .left,
                     backgroundColor: .primaryCTA,
                     highlightedBackgroundColor: .primaryCTA,
                     disabledBackgroundColor: .grey,
                     cornerRadius: 15.0,
                     fontColor: .white,
                     highligtedFontColor: .white,
                     disabledFontColor: .black,
                     font: .boldSystemFont(ofSize: 16.0))
    }
    
    static var white: Style {
        return Style(alignment: .left,
                     backgroundColor: .white,
                     highlightedBackgroundColor: .white,
                     disabledBackgroundColor: .white,
                     cornerRadius: 5.0,
                     fontColor: .tertiary,
                     highligtedFontColor: .tertiary,
                     disabledFontColor: .tertiary,
                     font: .boldSystemFont(ofSize: 13.0))
    }
    
    static var secondary: Style {
        return Style(alignment: .left,
                     backgroundColor: .background,
                     highlightedBackgroundColor: .background,
                     disabledBackgroundColor: .grey,
                     cornerRadius: 15.0,
                     fontColor: .tertiary,
                     highligtedFontColor: .tertiary,
                     disabledFontColor: .black,
                     font: .boldSystemFont(ofSize: 16.0))
    }
    
}
