import Foundation
import UIKit

struct Text {
    
    static var labelHeading: Style {
        return Style(alignment: .left,
                     fontColor: .primary,
                     font: .boldSystemFont(ofSize: 16.0))
    }
    
    static var labelSubheading: Style {
        return Style(alignment: .left,
                     fontColor: .secondary,
                     font: .systemFont(ofSize: 13.0))
    }
    
    static var labelSubheadingRight: Style {
        return Style(alignment: .right,
                     fontColor: .secondary,
                     font: .systemFont(ofSize: 13.0))
    }
    
    static var label: Style {
        return Style(alignment: .left,
                     fontColor: .label,
                     font: .systemFont(ofSize: 13.0))
    }
    
    static var labelTertiary: Style {
        return Style(alignment: .left,
                     fontColor: .tertiary,
                     font: .systemFont(ofSize: 13.0))
    }
    
    static func labelWhite(withFont font: UIFont) -> Style {
        return Style(alignment: .left,
                     fontColor: UIColor.white,
                     highligtedFontColor: UIColor.white,
                     disabledFontColor: UIColor.white,
                     font: font)
    }
}
