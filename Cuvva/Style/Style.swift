import Foundation
import UIKit

struct Style: Equatable {
    let alignment: NSTextAlignment
    let buttonImage: UIImage?
    let borderColor: UIColor?
    let backgroundColor: UIColor?
    let highlightedBackgroundColor: UIColor?
    let disabledBackgroundColor: UIColor?
    let cornerRadius: CGFloat?
    let fontColor: UIColor
    let highligtedFontColor: UIColor?
    let disabledFontColor: UIColor?
    let lineHeight: CGFloat?
    let presentArrowIndiciator: Bool
    let font: UIFont
    
    init(alignment: NSTextAlignment,
         buttonImage: UIImage? = nil,
         borderColor: UIColor? = nil,
         backgroundColor: UIColor? = nil,
         highlightedBackgroundColor: UIColor? = nil,
         disabledBackgroundColor: UIColor? = nil,
         cornerRadius: CGFloat? = nil,
         fontColor: UIColor,
         highligtedFontColor: UIColor? = nil,
         disabledFontColor: UIColor? = nil,
         lineHeight: CGFloat? = nil,
         presentArrowIndiciator: Bool = false,
         font: UIFont) {
        
        self.alignment = alignment
        self.buttonImage = buttonImage
        self.borderColor = borderColor
        self.backgroundColor = backgroundColor
        self.highlightedBackgroundColor = highlightedBackgroundColor
        self.disabledBackgroundColor = disabledBackgroundColor
        self.cornerRadius = cornerRadius
        self.fontColor = fontColor
        self.highligtedFontColor = highligtedFontColor
        self.disabledFontColor = disabledFontColor
        self.lineHeight = lineHeight
        self.presentArrowIndiciator = presentArrowIndiciator
        self.font = font
    }
}
