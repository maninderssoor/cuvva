import Foundation
import UIKit

extension UIColor {
    static let primary = UIColor(red: 0.09, green: 0.09, blue: 0.34, alpha: 1.00) //#161656
    static let secondary = UIColor(red: 0.36, green: 0.36, blue: 0.68, alpha: 1.00) // #5d5dae
    static let tertiary = UIColor(red: 0.35, green: 0.33, blue: 1.00, alpha: 1.00) // #5a55ff
    static let label = UIColor(red: 0.74, green: 0.74, blue: 0.74, alpha: 1.00) // #bcbcbc
    static let darkIndigo = UIColor(red: 0.00, green: 0.02, blue: 0.55, alpha: 1.00) // #00068c
    static let primaryCTA = UIColor(red: 0.11, green: 0.78, blue: 0.55, alpha: 1.00) // #1CC68C
    static let warning = UIColor(red: 1.00, green: 0.84, blue: 0.20, alpha: 1.00) // #ffd734
    static let alert = UIColor(red: 0.99, green: 0.17, blue: 0.18, alpha: 1.00) // #fc5051
    static let grey = UIColor(red: 0.56, green: 0.56, blue: 0.62, alpha: 1.00) // #8e8e9e
    static let border = UIColor(red: 0.86, green: 0.87, blue: 0.88, alpha: 1.00) // #4243c4
    static let background = UIColor(red: 0.94, green: 0.93, blue: 1.00, alpha: 1.00) // #EFEDFF
}
