import Foundation

enum PolicyViewControllerLocalisers: String {
    case active = "PolicyViewControllerLocalisers.active"
    case vehicles = "PolicyViewControllerLocalisers.vehicles"
    case extend = "PolicyViewControllerLocalisers.extend"
    case insure = "PolicyViewControllerLocalisers.insure"
    case totalPolicies = "PolicyViewControllerLocalisers.totalPolicies"

    var localized: String {
        return self.rawValue.localized(tableName: "PolicyViewControllerLocalisers")
    }

}
