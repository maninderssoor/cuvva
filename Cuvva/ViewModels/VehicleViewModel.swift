import Foundation
import CoreData
import UIKit

class VehicleViewModel {

    let vehicle: Vehicle
    var activePolicies: [Policy]?
    var previousPolicies: [Policy]?
    let dateFormatter = DateFormatter()

    init(vehicle: Vehicle) {
        self.vehicle = vehicle
        
        if let policies = vehicle.policies?.toArray as? [Policy] {
            self.activePolicies = policies.filter({
                guard let startDate = $0.startDateTime,
                    let endDate = $0.endDateTime,
                    let utcDate = utcDate else {
                    return false
                }
                
                if startDate <= utcDate && endDate >= utcDate {
                    return true
                } else {
                    return false
                }
            })
            
            self.previousPolicies = policies.filter({
                guard let endDate = $0.endDateTime,
                    let utcDate = utcDate else {
                    return false
                }
                
                return endDate <= utcDate
            })
        }
        
        dateFormatter.dateFormat = "E,  dd MMM  YYYY"
        
    }
    
    var utcDate: Date? {
        // ¯\_(ツ)_/¯ v1 :(, need UTC date. Maybe in an extension :)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss z"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let nowUTCString = dateFormatter.string(from: Date())
        return dateFormatter.date(from: nowUTCString)
    }
    
    var make: String? {
        var header = ""
        
        if let make = vehicle.make {
            header = make
        }
        if let model = vehicle.model {
            if header != "" { header += " " }
            header += model
        }
        
        return header
    }
    
    var registration: String? {
        return vehicle.prettyVrm
    }
    
    var numberOfPolicies: String {
        guard let policies = vehicle.policies else { return "0" }
        return "\(policies.count)"
    }
    
    // MARK: UITableView
    
    var numberOfSections: Int {
        return 1
    }
    
    func numberOfRows(inSection section: Int) -> Int? {
       return previousPolicies?.count
    }
    
    var titleForHeader: String {
        return "Previous driving policies" // oh dear
    }
    
    func policy(atIndexPath indexPath: IndexPath) -> Policy? {
        guard let sorted = previousPolicies, indexPath.row < sorted.count else {
            return nil
        }
        
        return sorted[indexPath.row]
    }
    
    func duration(atIndexPath indexPath: IndexPath) -> String? {
        guard let policy = policy(atIndexPath: indexPath) else {
            return nil
        }
        
        guard let startTime = policy.startDateTime, let endTime = policy.endDateTime else { return nil }
        
        let differenceInhour = endTime.timeIntervalSince(startTime) / 60
        let differenceFormatted = String(format: "%.0f", differenceInhour)
        return "\(differenceFormatted) minutes remaining" // future dev, can't always assume a min, formatting can be done better
    }
    
    func date(atIndexPath indexPath: IndexPath) -> String? {
        guard let policy = policy(atIndexPath: indexPath), let start = policy.startDateTime else {
            return nil
        }
        
        return dateFormatter.string(from: start)
    }

    func makeImage() -> UIImage? {
        var image = UIImage(named: "car")
        if let make = vehicle.make?.lowercased(),
            let vehicleMakeImage = UIImage(named: make) {
            image = vehicleMakeImage
        }
        
        return image
    }
}
