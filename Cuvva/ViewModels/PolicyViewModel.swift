import Foundation
import CoreData
import UIKit

class PolicyViewModel {

    let networkManager: Networking
    let databaseManager: Database

    var activePolicies: [Vehicle]?
    var lapsedPolicies: [Vehicle]?

    init(networkManager: Networking = NetworkManager(), databaseManager: Database = CoreDataManager()) {
        self.networkManager = networkManager
        self.databaseManager = databaseManager
        databaseManager.setup()

        self.startObserving()
    }

    deinit {
        stopObserving()
    }

    // MARK: Product Data

    func update() {
        fetchFromAPI()
        fetchFromDatabase()
    }

    func fetchFromAPI(completion: (([APIPolicy]?) -> Void)? = nil) {
        let parameters = APIPolicyParameters()
        networkManager.fetch(with: APIPolicyService.self, and: parameters, and: APIPolicyResponse.self) { [weak self] result in
            guard let self = self else { return }
            guard let response = result.response as? APIPolicyResponse, result.isSuccess else {
                        NotificationCenter.default.post(name: Notifications.productsLoaded, object: nil)
                        completion?(nil)
                        return
            }

            let adapter = APIPolicyAdapter()
            
            var transactions = [Transaction]()
            for policy in response.policies {
                guard policy.transactionPayload != nil,
                    let transactionToInsert = adapter.toTransaction(with: policy) else { continue }
                
                let transactionpredicate = NSPredicate(format: "\(TransactionKeys.uuid.rawValue) = %@", policy.uuid)
                if let dbTransaction = self.databaseManager.save(in: TransactionKeys.entity.rawValue, with: transactionToInsert, overwritePredicate: true, overwritePredicate: transactionpredicate) as? Transaction {
                    transactions.append(dbTransaction)
                }
            }
            
            for policy in response.policies {
                guard let newPolicy = policy.policyPayload,
                    var policyToInsert = adapter.toPolicy(with: policy) else { continue }

                let vehiclePredicate = NSPredicate(format: "\(VehicleKeys.vrm.rawValue) = %@", newPolicy.vrm)
                if let vehicleToInsert = adapter.toVehicle(with: policy),
                    let dbVehicle = self.databaseManager.save(in: VehicleKeys.entity.rawValue, with: vehicleToInsert, overwritePredicate: true, overwritePredicate: vehiclePredicate) {
                    policyToInsert[PolicyKeys.vehicle.rawValue] = dbVehicle
                }
                
                if let dbTransaction = transactions.filter({ $0.policyId == policy.policyPayload?.policy_id }).first {
                    policyToInsert[PolicyKeys.transaction.rawValue] = dbTransaction
                }
                
                let policyPredicate = NSPredicate(format: "\(PolicyKeys.uuid.rawValue) = %@", policy.uuid)
                self.databaseManager.save(in: PolicyKeys.entity.rawValue, with: policyToInsert, overwritePredicate: true, overwritePredicate: policyPredicate)
            }
            
            self.databaseManager.save()
            completion?(response.policies)
        }
    }

    func fetchFromDatabase() {
        guard let nowUTC = utcDate else { return }
        
        let policySort = NSSortDescriptor(key: VehicleKeys.make.rawValue, ascending: true)
        let policyPredicate = NSPredicate(format: "ANY \(VehicleKeys.policies.rawValue).\(PolicyKeys.startDateTime.rawValue) <= %@ AND ANY \(VehicleKeys.policies.rawValue).\(PolicyKeys.endDateTime.rawValue) >= %@", argumentArray: [nowUTC, nowUTC])
        if let vehicles = databaseManager.fetch(for: VehicleKeys.entity.rawValue, filter: policyPredicate, sorting: [policySort], with: nil) as? [Vehicle] {
            activePolicies = vehicles
        }
        
        let lapsedPolicyPredicate = NSPredicate(format: "ANY \(VehicleKeys.policies.rawValue).\(PolicyKeys.endDateTime.rawValue) < %@", argumentArray: [nowUTC])
        if let vehicles = databaseManager.fetch(for: VehicleKeys.entity.rawValue, filter: lapsedPolicyPredicate, sorting: [policySort], with: nil) as? [Vehicle] {
            lapsedPolicies = vehicles
        }
    }

    func hasData() -> Bool {
        let activeCount = activePolicies?.count ?? 0
        let lapsedPoliciesCount = lapsedPolicies?.count ?? 0
        
        return activeCount > 0 || lapsedPoliciesCount > 0
    }
    
    var utcDate: Date? {
        // ¯\_(ツ)_/¯ v1 :(, need UTC date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss z"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let nowUTCString = dateFormatter.string(from: Date())
        return dateFormatter.date(from: nowUTCString)
    }

    // MARK: Core Data

    func startObserving() {
        NotificationCenter.default.addObserver(self, selector: #selector(databaseDidSave), name: Notification.Name.NSManagedObjectContextDidSave, object: CoreDataManager.instance.persistentContainer.viewContext)
    }

    func stopObserving() {
        NotificationCenter.default.removeObserver(self, name: Notification.Name.NSManagedObjectContextDidSave, object: nil)
    }

    @objc func databaseDidSave() {
		fetchFromDatabase()
        UserDefaults.standard.set(true, forKey: Defaults.isFirstLoadComplete.rawValue)
        NotificationCenter.default.post(name: Notifications.productsLoaded, object: nil)
    }

    // MARK: TableView View

    var numberOfSections: Int {
        return hasData() ? 2 : 0
    }

    func numberOfRows(inSection section: Int) -> Int {
        switch section {
        case 0:
            guard let policies = activePolicies else { return 0 }
            return policies.count
        
        case 1:
            guard let policies = lapsedPolicies else { return 0 }
            return policies.count
            
        default:
            return 0
        }
    }

    func titleForHeader(inSection section: Int) -> String? {
        switch section {
        case 0:
            return PolicyViewControllerLocalisers.active.localized
        case 1:
            return PolicyViewControllerLocalisers.vehicles.localized
        default:
            return nil
        }
    }
    
    func heightForRow(atIndexPath indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return PolicyLayoutManager.activeCellHeight.rawValue
        case 1:
            return PolicyLayoutManager.lapsedCellHeight.rawValue
        default:
            return 0.0
        }
    }
    
    func vehicle(atIndexPath indexPath: IndexPath) -> Vehicle? {
        switch indexPath.section {
        case 0:
            guard let policies = activePolicies, indexPath.row < policies.count else { return  nil }
            return policies[indexPath.row]
        
        case 1:
            guard let policies = lapsedPolicies, indexPath.row < policies.count else { return  nil }
            return policies[indexPath.row]
            
        default:
            return nil
        }
    }

    func make(atIndexPath indexPath: IndexPath) -> String? {
        guard let vehicle = vehicle(atIndexPath: indexPath) else { return  nil }
        
        return vehicle.make
    }
    
    func makeImage(atIndexPath indexPath: IndexPath) -> UIImage? {
        guard let vehicle = vehicle(atIndexPath: indexPath) else { return  nil }
        
        var image = UIImage(named: "car")
        if let make = vehicle.make?.lowercased(),
            let vehicleMakeImage = UIImage(named: make) {
            image = vehicleMakeImage
        }
        
        return image
    }
    
    func model(atIndexPath indexPath: IndexPath) -> String? {
        guard let vehicle = vehicle(atIndexPath: indexPath) else { return  nil }
        
        var model = ""
        if let color = vehicle.color {
            model = color
        }
        if let vehicleModel = vehicle.model {
            if model != "" { model += " " }
            model += vehicleModel
        }
        return model
    }
    
    func registration(atIndexPath indexPath: IndexPath) -> String? {
        guard let vehicle = vehicle(atIndexPath: indexPath) else { return  nil }
        
        return vehicle.prettyVrm
    }
    
    func numberOfPolicies(atIndexPath indexPath: IndexPath) -> String? {
        guard let vehicle = vehicle(atIndexPath: indexPath), let policies = vehicle.policies?.count else { return  nil }
        
        return "\(policies)"
    }
    
    func timeRemainingOnPolicy(atIndexPath indexPath: IndexPath) -> String? {
        let now = Date()
        guard let vehicle = vehicle(atIndexPath: indexPath),
            let policies = vehicle.policies?.toArray as? [Policy],
            let activePolicy = policies.filter({
                guard let endDatetime = $0.endDateTime else { return false }
                return endDatetime > now
            }).first else { return  nil }
        
        guard let timeIntervalDifference = activePolicy.endDateTime?.timeIntervalSinceNow else { return nil }
        let differenceInhour = timeIntervalDifference / 60
        let differenceFormatted = String(format: "%.0f", differenceInhour)
        return "\(differenceFormatted) minutes remaining" // future dev, can't always assume a min, formatting can be done better
    }
}
