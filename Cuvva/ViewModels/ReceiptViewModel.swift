//
//  ReceiptViewModel.swift
//  Cuvva
//
//  Created by Maninder Soor on 06/08/2020.
//  Copyright © 2020 Maninder. All rights reserved.
//

import Foundation


class ReceiptViewModel {

    let policy: Policy
    let penceInPounds = NSDecimalNumber(decimal: 100)
    let dateFormatter = DateFormatter()
    let formatter = NumberFormatter()

    init(policy: Policy) {
        self.policy = policy
        
        dateFormatter.dateFormat = "E,  dd MMM  YYYY"
        formatter.numberStyle = .currency
        formatter.currencyCode = "GBP"
        formatter.currencySymbol = "£"
    }
    
    // MARK: UITableView
    
    var numberOfSections: Int {
        return 1
    }
    
    func numberOfRows(inSection section: Int) -> Int {
        return 4
    }
    
    func titleForHeader (inSection section: Int) -> String {
        guard let startTime = policy.startDateTime else { return "" }
        
        return dateFormatter.string(from: startTime)
    }
    
    func label(forIndexPath indexPath: IndexPath) -> String {
        if indexPath.row == 0 {
            return "Insurance premium"
        } else if indexPath.row == 1 {
            return "Insurance premium tax"
        } else if indexPath.row == 2 {
            return "Admin fee"
        } else {
            return "Total Paid"
        }
    }
    
    func content(forIndexPath indexPath: IndexPath) -> String? {
        switch indexPath.row {
        case 0:
            return premium(atIndexPath: indexPath)
        case 1:
            return tax(atIndexPath: indexPath)
        case 2:
            return admin(atIndexPath: indexPath)
        case 3:
            return total(atIndexPath: indexPath)
        default:
            return nil
        }
    }
    
    func premium(atIndexPath indexPath: IndexPath) -> String? {
        guard let premium = policy.transaction?.premium else { return nil }
        
        return formatter.string(from: premium.dividing(by: penceInPounds))
    }
    
    func tax(atIndexPath indexPath: IndexPath) -> String? {
        guard let tax = policy.transaction?.premiumTax else { return nil }
        
        return formatter.string(from: tax.dividing(by: penceInPounds))
    }
    
    func admin(atIndexPath indexPath: IndexPath) -> String? {
        guard let adminFee = policy.transaction?.adminFee else { return nil }
        
        return formatter.string(from: adminFee.dividing(by: penceInPounds))
    }
    
    func total(atIndexPath indexPath: IndexPath) -> String? {
        guard let premium = policy.transaction?.premium,
            let tax = policy.transaction?.premiumTax,
            let adminFee = policy.transaction?.adminFee  else { return nil }
        
        let total = premium.adding(tax).adding(adminFee)
        return formatter.string(from: total.dividing(by: penceInPounds))
    }
}
