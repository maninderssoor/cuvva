import Foundation
import UIKit

class ReceiptViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    static let storyboardID = "ReceiptViewController"

    var viewModel: ReceiptViewModel?
    
    @IBOutlet weak var tableView: UITableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        setupNavigation()
        setupTableViews()
    }
    
    func setupView() {
        view?.backgroundColor = .background
    }
    
    func setupTableViews() {
        tableView?.backgroundColor = .background
    }
    
    func configure(with policy: Policy) {
        viewModel = ReceiptViewModel(policy: policy)
    }
    
    func setupNavigation() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.prefersLargeTitles = false

        let backButton = UIButton(type: .custom)
        backButton.frame = CGRect(x: 0, y: 0, width: 44.0, height: 44.0)
        backButton.addTarget(self, action: #selector(close), for: .touchUpInside)
        backButton.setImage(#imageLiteral(resourceName: "Arrow_Back"), for: .normal)
        backButton.setImage(#imageLiteral(resourceName: "Arrow_Back"), for: .highlighted)
        backButton.setImage(#imageLiteral(resourceName: "Arrow_Back"), for: .disabled)
        backButton.setTitle(nil, for: .normal)
        backButton.setTitle(nil, for: .highlighted)
        backButton.setTitle(nil, for: .disabled)

        let leftBarButton = UIBarButtonItem(customView: backButton)
        navigationItem.leftBarButtonItem = leftBarButton
    }
    
    @IBAction func close() {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: UITableView Datasource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel?.numberOfSections ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.numberOfRows(inSection: section) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PreviousPolicyCell.identifier, for: indexPath) as? PreviousPolicyCell else {
            preconditionFailure()
        }
        
        cell.labelDuration?.text = viewModel?.label(forIndexPath: indexPath)
        cell.labelDate?.text = viewModel?.content(forIndexPath: indexPath)
        
        return cell
    }
    
    // MARK: UITableView Delegate
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel?.titleForHeader(inSection: section)
    }
}
