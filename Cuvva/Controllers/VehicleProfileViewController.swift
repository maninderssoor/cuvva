import Foundation
import UIKit

class VehicleProfileViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    static let storyboardID = "VehicleProfileViewController"

    var viewModel: VehicleViewModel?
    
    @IBOutlet weak var imageLogo: UIImageView?
    @IBOutlet weak var labelMake: UILabel?
    @IBOutlet weak var labelRegistration: UILabel?
    @IBOutlet weak var labelNumberOfPoliciesTitle: UILabel?
    @IBOutlet weak var labelNumberOfPolicies: UILabel?
    @IBOutlet weak var tableView: UITableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        setupImageViews()
        setupLabels()
        setupTableViews()
    }
    
    func setupView() {
        view?.backgroundColor = .background
    }
    
    func setupImageViews() {
        imageLogo?.layer.cornerRadius = 25.0
        imageLogo?.layer.masksToBounds = true
    }
    
    func setupLabels() {
        labelMake?.style(given: Text.labelWhite(withFont: .systemFont(ofSize: 16.0)))
        labelRegistration?.style(given: Text.labelWhite(withFont: .boldSystemFont(ofSize: 20.0)))
        
        labelNumberOfPolicies?.style(given: Text.labelWhite(withFont: .systemFont(ofSize: 13.0)))
    }
    
    func setupTableViews() {
        tableView?.backgroundColor = .background
    }
    
    func configure(with vehicle: Vehicle) {
        viewModel = VehicleViewModel(vehicle: vehicle)
        
        labelMake?.text = viewModel?.make
        labelRegistration?.text = viewModel?.registration
        labelNumberOfPolicies?.text = viewModel?.numberOfPolicies
        imageLogo?.image = viewModel?.makeImage()
        
        tableView?.reloadData()
        
        UIView.animate(withDuration: 0.3) {[weak self] in
            self?.labelMake?.alpha = 1.0
            self?.labelRegistration?.alpha = 1.0
            self?.labelNumberOfPoliciesTitle?.alpha = 1.0
            self?.labelNumberOfPolicies?.alpha = 1.0
            self?.imageLogo?.alpha = 1.0
        }
    }
    
    @IBAction func close() {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: UITableView Datasource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel?.numberOfSections ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.numberOfRows(inSection: section) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PreviousPolicyCell.identifier, for: indexPath) as? PreviousPolicyCell else {
            preconditionFailure()
        }
        
        cell.labelDuration?.text = viewModel?.duration(atIndexPath: indexPath)
        cell.labelDate?.text = viewModel?.date(atIndexPath: indexPath)
        cell.labelDate?.textAlignment = .right
        
        return cell
    }
    
    // MARK: UITableView Delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let controller = UIStoryboard(name: ReceiptViewController.storyboardID, bundle: nil).instantiateInitialViewController() as? ReceiptViewController,
            let policy = viewModel?.policy(atIndexPath: indexPath) else {
                return
        }
        
        navigationController?.pushViewController(controller, animated: true)
        controller.configure(with: policy)
    }
}
