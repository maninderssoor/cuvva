import Foundation
import UIKit

class NavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        setupAppearance()
        setupSwipe()
        setControllers()
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    func setupAppearance() {
        navigationBar.shadowImage = UIImage()
    }

    func setupSwipe() {
        interactivePopGestureRecognizer?.delegate = self
    }

    func setControllers() {
        guard viewControllers.count == 0 else { return }
        
        guard let viewController = UIStoryboard(name: PolicyViewController.storyboardID, bundle: nil).instantiateInitialViewController() else { return }
        setViewControllers([viewController], animated: false)
    }
}

extension NavigationController: UIGestureRecognizerDelegate {

    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
