import Foundation
import UIKit

// MARK: UITableView Datasource
extension PolicyViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows(inSection: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ActivePolicyCell.cellIdentifier, for: indexPath) as? ActivePolicyCell else {
                preconditionFailure()
            }
            
            cell.setMake(with: viewModel.makeImage(atIndexPath: indexPath))
            cell.labelMake?.text = viewModel.make(atIndexPath: indexPath)
            cell.labelModel?.text = viewModel.model(atIndexPath: indexPath)
            cell.labelRegistration?.text = viewModel.registration(atIndexPath: indexPath)
            cell.labelNumberOfPolicies?.text = viewModel.numberOfPolicies(atIndexPath: indexPath)
            cell.labelTimeRemaining?.text = viewModel.timeRemainingOnPolicy(atIndexPath: indexPath)
            
            return cell
        
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: PolicyCell.identifier, for: indexPath) as? PolicyCell else {
                preconditionFailure()
            }
            
            cell.setMake(with: viewModel.makeImage(atIndexPath: indexPath))
            cell.labelMake?.text = viewModel.make(atIndexPath: indexPath)
            cell.labelModel?.text = viewModel.model(atIndexPath: indexPath)
            cell.labelRegistration?.text = viewModel.registration(atIndexPath: indexPath)
            cell.labelNumberOfPolicies?.text = viewModel.numberOfPolicies(atIndexPath: indexPath)
            
            return cell
        default:
            return UITableViewCell()
        }
    }
}
