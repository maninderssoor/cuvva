import Foundation
import UIKit

// MARK: UITableView Delegate
extension PolicyViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let controller = UIStoryboard(name: VehicleProfileViewController.storyboardID, bundle: nil).instantiateInitialViewController() as? NavigationController,
            let vehicleController = controller.viewControllers.first as? VehicleProfileViewController,
            let vehicle = viewModel.vehicle(atIndexPath: indexPath) else {
                return
        }
        
        controller.modalPresentationStyle = .fullScreen
        present(controller, animated: true) {
            vehicleController.configure(with: vehicle)
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.titleForHeader(inSection: section)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return PolicyLayoutManager.headerHeight.rawValue
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.heightForRow(atIndexPath: indexPath)
    }

}

