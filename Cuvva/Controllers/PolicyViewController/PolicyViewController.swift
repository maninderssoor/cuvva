import Foundation
import UIKit

class PolicyViewController: UIViewController {

    static let storyboardID = "PolicyViewController"

    var viewModel = PolicyViewModel()

    @IBOutlet weak var buttonMotor: UIButton?
    @IBOutlet weak var buttonTravel: UIButton?
    @IBOutlet weak var activityLoader: UIActivityIndicatorView?
    @IBOutlet weak var labelNoPolicies: UILabel?
    @IBOutlet weak var tableView: UITableView?

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        setupTableView()
        setupButtons()
        loadData()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        startObserving()

        if UserDefaults.standard.value(forKey: Defaults.isFirstLoadComplete.rawValue) != nil {
            setupData()
        }
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        stopObserving()
    }

    // MARK: Setup

    func setupView() {
        view.accessibilityIdentifier = AccessibilityIdentifiers.policyViewController.view.identifier
        view?.backgroundColor = .background
        
        labelNoPolicies?.accessibilityIdentifier = AccessibilityIdentifiers.policyViewController.noPolicies.identifier
        activityLoader?.accessibilityIdentifier = AccessibilityIdentifiers.policyViewController.activity.identifier
    }

    func setupTableView() {
        tableView?.accessibilityIdentifier = AccessibilityIdentifiers.policyViewController.tableView.identifier
        tableView?.register(UINib(nibName: ActivePolicyCell.cellIdentifier, bundle: nil), forCellReuseIdentifier: ActivePolicyCell.cellIdentifier)
        tableView?.register(UINib(nibName: PolicyCell.identifier, bundle: nil), forCellReuseIdentifier: PolicyCell.identifier)
        tableView?.backgroundColor = .background
    }

    func setupButtons() {
        buttonMotor?.style(given: Button.white)
        buttonTravel?.style(given: Button.white)
    }
    
    func setupData() {
        let hasData = viewModel.hasData()
        tableView?.reloadData()

        UIView.animateKeyframes(withDuration: 0.3, delay: 0.0, options: .beginFromCurrentState, animations: {[weak self] in
            guard let self = self else { return }

            self.activityLoader?.alpha = 0.0
            self.labelNoPolicies?.alpha = hasData ? 0.0 : 1.0
            self.tableView?.alpha = hasData ? 1.0 : 0.0
        }, completion: nil)
    }

    func loadData() {
        viewModel.update()
    }

    // MARK: Listeners

    func startObserving() {
        NotificationCenter.default.addObserver(self, selector: #selector(didUpdatePolicies), name: Notifications.productsLoaded, object: nil)
    }

    func stopObserving() {
        NotificationCenter.default.removeObserver(self, name: Notifications.productsLoaded, object: nil)
    }

    @objc func didUpdatePolicies() {
		setupData()
    }
}
