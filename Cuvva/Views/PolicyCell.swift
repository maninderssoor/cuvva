import Foundation
import UIKit

class PolicyCell: UITableViewCell {

    static let identifier = "PolicyCell"

    @IBOutlet weak var viewBackground: UIView?
    @IBOutlet weak var imageMake: UIImageView?
    @IBOutlet weak var labelMake: UILabel?
    @IBOutlet weak var labelModel: UILabel?
    @IBOutlet weak var labelRegistration: UILabel?
    @IBOutlet weak var labelNumberOfPolicies: UILabel?
    @IBOutlet weak var buttonInsure: UIButton?

    // MARK: Lifecycle

    override func awakeFromNib() {
        super.awakeFromNib()

        setup()
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        imageMake?.image = nil
        labelMake?.text = nil
        labelModel?.text = nil
        labelRegistration?.text = nil
        labelNumberOfPolicies?.text = nil
    }

    // MARK: Setup

    func setup() {
        contentView.backgroundColor = .clear
        
        viewBackground?.backgroundColor = .white
        viewBackground?.layer.borderColor = UIColor.border.cgColor
        viewBackground?.layer.borderWidth = 1.0
        
        viewBackground?.layer.cornerRadius = 8.0
        viewBackground?.layer.masksToBounds = true
        
        labelMake?.style(given: Text.labelHeading)
        labelModel?.style(given: Text.labelSubheading)
        labelRegistration?.style(given: Text.label)
        labelNumberOfPolicies?.style(given: Text.label)
        
        buttonInsure?.style(given: Button.secondary)
    }

    func setMake(with image: UIImage?) {
        imageMake?.image = image?.withRenderingMode(.alwaysTemplate)
        imageMake?.tintColor = .primary
    }
}
