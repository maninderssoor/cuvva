import Foundation
import UIKit

class PreviousPolicyCell: UITableViewCell {

    static let identifier = "PreviousPolicyCell"

    @IBOutlet weak var labelDuration: UILabel?
    @IBOutlet weak var labelDate: UILabel?

    // MARK: Lifecycle

    override func awakeFromNib() {
        super.awakeFromNib()

        setup()
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        labelDuration?.text = nil
        labelDate?.text = nil
    }

    // MARK: Setup

    func setup() {
        labelDuration?.style(given: Text.labelSubheading)
        labelDate?.style(given: Text.labelSubheadingRight)
    }
}
