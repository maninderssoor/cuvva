import Foundation
import UIKit

class ActivePolicyCell: PolicyCell {
    
    static let cellIdentifier = "ActivePolicyCell"
    
    @IBOutlet weak var labelTimeRemaining: UILabel?
    @IBOutlet weak var buttonExtend: UIButton?
    
    override func setup() {
        super.setup()
        
        labelTimeRemaining?.style(given: Text.labelTertiary)
        buttonExtend?.style(given: Button.default)
    }
}
