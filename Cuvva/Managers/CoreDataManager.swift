import Foundation
import CoreData

protocol Database {
    func setup()
    func save()

    var persistentContainer: NSPersistentContainer { get set }
    var privateContext: NSManagedObjectContext { get set }

    func fetch(for table: String, filter predicate: NSPredicate?, sorting sort: [NSSortDescriptor]?, with context: NSManagedObjectContext?) -> [NSManagedObject]?
    func delete(in table: String)
    @discardableResult
    func save(in table: String, with parameters: [String: Any], overwritePredicate overrite: Bool, overwritePredicate predicate: NSPredicate?) -> NSManagedObject?
}

class CoreDataManager: Database {

    static let instance = CoreDataManager()

    var privateContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)

    //MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Cuvva")
        container.loadPersistentStores { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        }

        return container
    }();

    //MARK: - Core Data saving support

    func save() {
        let context = CoreDataManager.instance.persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                print("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

    func setup() {
        privateContext.parent = CoreDataManager.instance.persistentContainer.viewContext
    }

    // MARK: Generic Functions

    func fetch(for table: String, filter predicate: NSPredicate?, sorting sort: [NSSortDescriptor]?, with context: NSManagedObjectContext?) -> [NSManagedObject]? {
        var finalContext = CoreDataManager.instance.persistentContainer.viewContext
        if let context = context {
            finalContext = context
        }
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: table)
        
        if let predicate = predicate {
            request.predicate = predicate
        }
        
        if let sorting = sort {
            request.sortDescriptors = sorting
        }
        
        do {
            let fetched = try finalContext.fetch(request)
            
            print("[CoreDataManager] Fetched \(table): \(fetched.count) from CoreData.")
            return fetched as? [NSManagedObject]
        } catch {
            print("[CoreDataManager] Could not fetch \(table) from CoreData.")
            return nil
        }
    }

    func delete(in table: String) {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: table)
        do {
            guard let objects = try CoreDataManager.instance.persistentContainer.viewContext.fetch(request) as? [NSManagedObject] else { return }

            for object in objects {
                CoreDataManager.instance.persistentContainer.viewContext.delete(object)
            }

            save()
            print("[CoreDataManager] Deleted \(objects.count) items in \(table)")
        } catch {
            print("[CoreDataManager] Couldn't delete items from \(table)")
            return
        }
    }

    @discardableResult
    func save(in table: String, with parameters: [String: Any], overwritePredicate overrite: Bool, overwritePredicate predicate: NSPredicate?) -> NSManagedObject? {
        guard let entityDescription = NSEntityDescription.entity(forEntityName: table, in: privateContext) else {
            print("[CoreDataManager] Couldn't initialise an entity description for insertion given entity name \(table)")
            return nil
        }
        
        var object: NSManagedObject?
        
        if let predicate = predicate,
            let exisitingObject = fetch(for: table, filter: predicate, sorting: nil, with: privateContext)?.first, overrite {
            object = exisitingObject
        } else {
            object = NSManagedObject(entity: entityDescription, insertInto: privateContext)
        }
        
        guard let newObject = object else {
            print("[CoreDataManager] Couldn't unwrap a new/ exisiting object")
            return nil
        }
        
        // Set the parameteers
        for parameter in parameters {
            newObject.setValue(parameter.value, forKey: parameter.key)
        }

        do {
            try privateContext.save()
            return newObject
        } catch {
            let nserror = error as NSError
            print("Unresolved error \(nserror), \(nserror.userInfo)")
            return nil
        }
    }
}
