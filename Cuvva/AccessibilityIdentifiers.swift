import Foundation

enum AccessibilityIdentifiers {

    enum policyViewController: String {
		case view = "Policy View Controller"
        case activity = "Policy Activity Indicator View"
        case noPolicies = "No Policies"
        case tableView = "Policy Table View"

        var identifier: String {
            return self.rawValue
        }
    }
}


enum UIConstants: String {
    case test = "Test"
    case test_no_products
    case test_single_product
}
