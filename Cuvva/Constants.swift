import Foundation

// Could be made into Environmental Constants based on configuration
enum Constants: String {
    case baseURL = "https://cuvva.herokuapp.com/"
}

enum Defaults: String {
    case isFirstLoadComplete = "isFirstLoadComplete"
}

struct Notifications {
    static let productsLoaded = NSNotification.Name("Products Loaded")
}
