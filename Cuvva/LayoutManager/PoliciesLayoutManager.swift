import Foundation
import UIKit

enum PolicyLayoutManager: CGFloat {
    case headerHeight = 60
    case activeCellHeight = 160.0
    case lapsedCellHeight = 130.0
}
