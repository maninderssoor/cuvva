import Foundation
import UIKit

extension UIButton {
    private func imageWithColor(color: UIColor?) -> UIImage? {
        guard let color = color else { return nil }
        let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
        UIGraphicsBeginImageContext(rect.size)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }

        context.setFillColor(color.cgColor)
        context.fill(rect)

        guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
            return nil
        }
        UIGraphicsEndImageContext()

        return image
    }
    
    func style(given style: Style) {
        backgroundColor = style.backgroundColor
        
        setBackgroundImage(imageWithColor(color: style.backgroundColor), for: .normal)
        setBackgroundImage(imageWithColor(color: style.highlightedBackgroundColor), for: .highlighted)
        setBackgroundImage(imageWithColor(color: style.disabledBackgroundColor), for: .disabled)
        
        setTitleColor(style.fontColor, for: .normal)
        setTitleColor(style.highligtedFontColor, for: .highlighted)
        setTitleColor(style.disabledFontColor, for: .disabled)
        
        titleLabel?.font = style.font
        titleLabel?.textAlignment = style.alignment
        
        if let cornerRadius = style.cornerRadius {
            layer.masksToBounds = true
            layer.cornerRadius = cornerRadius
        }
        
        if let borderColor = style.borderColor {
            layer.borderColor = borderColor.cgColor
            layer.borderWidth = 2.0
        }
        
        if let image = style.buttonImage, style.presentArrowIndiciator {
            setImage(image, for: .normal)
            setImage(image, for: .highlighted)
            setImage(image, for: .disabled)
            
            switch style.alignment {
            case .left:
                titleEdgeInsets.left = image.size.width
                titleEdgeInsets.right = -image.size.width
            case .right:
                titleEdgeInsets.left = -image.size.width
                titleEdgeInsets.right = image.size.width
                if let size = titleLabel?.frame.size {
                    imageEdgeInsets.left = size.width
                    imageEdgeInsets.right = -size.width
                }
            default:
                break
            }
        }
    }
}
