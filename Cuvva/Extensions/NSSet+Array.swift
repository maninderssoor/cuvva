import Foundation

extension NSSet {
    
    var toArray: Array<Any> {
        return Array(self)
    }
}
