import Foundation
import UIKit

extension UILabel {
    
    func style(given style: Style) {
        font = style.font
        textColor = style.fontColor
        textAlignment = style.alignment
        numberOfLines = 0
        
        if let lineHeight = style.lineHeight, let text = text {
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = lineHeight
			paragraphStyle.alignment = style.alignment

            let attrString = NSMutableAttributedString(string: text)
            attrString.addAttribute(.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attrString.length))
            self.text = nil
            attributedText = attrString
        }
    }
}
