import Foundation

enum APIPolicyType: String {
    case created = "policy_created"
    case transaction = "policy_financial_transaction"
}

struct APIPolicyResponse: APIProtocolResponse {
    let policies: [APIPolicy]
}

extension APIPolicyResponse {

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        
        self.policies = try container.decode([APIPolicy].self)
    }
}

struct APIPolicy: Decodable {
    let uuid: String
    let timestamp: String
    
    let policyPayload: APIPolicyPayload?
    let transactionPayload: APITransactionPayload?

    enum CodingKeys: String, CodingKey {
        case type
        case timestamp
        case unique_key
        case payload
    }
}

extension APIPolicy {
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.timestamp = try container.decode(String.self, forKey: .timestamp)
        self.uuid = try container.decode(String.self, forKey: .unique_key)
        
        let type = try container.decode(String.self, forKey: .type)
        if type == APIPolicyType.created.rawValue {
            self.policyPayload = try container.decode(APIPolicyPayload.self, forKey: .payload)
        } else {
            self.policyPayload = nil
        }
        
        if type == APIPolicyType.transaction.rawValue {
            self.transactionPayload = try container.decode(APITransactionPayload.self, forKey: .payload)
        } else {
            self.transactionPayload = nil
        }
    }
}

struct APIPolicyPayload: Decodable {
    let user_id: String

    let policy_id: String
    let original_policy_id: String
    let reference_code: String
    let start_date: String
    let end_date: String
    let incident_phone: String
    
    let vrm: String
    let prettyVrm: String
    let make: String
    let model: String
    let color: String
    
    enum CodingKeys: String, CodingKey {
        case user_id
        case policy_id
        case original_policy_id
        case reference_code
        case start_date
        case end_date
        case incident_phone
        case vehicle
    }
    
    enum VehicleCodingKeys: String, CodingKey {
        case vrm
        case prettyVrm
        case make
        case model
        case color
    }
}

extension APIPolicyPayload {
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.user_id = try container.decode(String.self, forKey: .user_id)
        self.policy_id = try container.decode(String.self, forKey: .policy_id)
        self.original_policy_id = try container.decode(String.self, forKey: .original_policy_id)
        self.reference_code = try container.decode(String.self, forKey: .reference_code)
        self.start_date = try container.decode(String.self, forKey: .start_date)
        self.end_date = try container.decode(String.self, forKey: .end_date)
        self.incident_phone = try container.decode(String.self, forKey: .incident_phone)
        
        let vehicle = try container.nestedContainer(keyedBy: VehicleCodingKeys.self, forKey: .vehicle)
        self.vrm = try vehicle.decode(String.self, forKey: .vrm)
        self.prettyVrm = try vehicle.decode(String.self, forKey: .prettyVrm)
        self.make = try vehicle.decode(String.self, forKey: .make)
        self.model = try vehicle.decode(String.self, forKey: .model)
        self.color = try vehicle.decode(String.self, forKey: .color)
    }
}

struct APITransactionPayload: Decodable {
    let policy_id: String
    
    let total_premium: Int
    let ipt: Int
    let extra_fees: Int
    let total_payable: Int
    
    enum CodingKeys: String, CodingKey {
        case policy_id
        case pricing
    }
    
    enum PricingCodingKeys: String, CodingKey {
        case total_premium
        case ipt
        case extra_fees
        case total_payable
    }

}

extension APITransactionPayload {
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.policy_id = try container.decode(String.self, forKey: .policy_id)
        
        let pricing = try container.nestedContainer(keyedBy: PricingCodingKeys.self, forKey: .pricing)
        self.total_premium = try pricing.decode(Int.self, forKey: .total_premium)
        self.extra_fees = try pricing.decode(Int.self, forKey: .extra_fees)
        self.total_payable = try pricing.decode(Int.self, forKey: .total_payable)
        self.ipt = try pricing.decode(Int.self, forKey: .ipt)
    }
}
