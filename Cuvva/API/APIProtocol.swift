import Foundation

protocol APIProtocol: Decodable {
	static var slug: String { get set }
	static var title: String { get }
    static var method: APIMethod { get }
}

protocol APIProtocolParameters: Encodable {
	func parameters() -> Data?
}

extension APIProtocolParameters {
    func parameters() -> Data? {
        var encoder: JSONEncoder?
        encoder = JSONEncoder()
        
        guard let jsonData = try? encoder?.encode(self) else {
            encoder = nil
            return nil
        }
        encoder = nil
        
        return jsonData
    }
    
}

protocol APIProtocolResponse: Decodable {}
