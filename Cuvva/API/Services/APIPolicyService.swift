import Foundation

struct APIPolicyService: APIProtocol {
    static var title: String = "Fetch Policies"
    static var method: APIMethod = .get
    static var slug: String = ""
}
