import Foundation

/**
	An API Progress object when the API call conforms to APIProtocol
*/
struct APIProtocolCompletion {
	let isSuccess: Bool
    let request: URLRequest?
    let service: APIProtocol.Type?
    let response: APIProtocolResponse?
	let httpResponse: HTTPURLResponse?
	let httpResponseCode: HTTPStatusCode?
	let error: Error?
	
	/**
		Basic Initialiser
	*/
    init(isSuccess: Bool, request: URLRequest? = nil, service: APIProtocol.Type? = nil, response: APIProtocolResponse? = nil, httpResponse: HTTPURLResponse? = nil, httpResponseCode: Int? = nil, error: Error? = nil, urlErrorCode: Int? = nil, urlErrorDescription: String? = nil) {
		self.isSuccess = isSuccess
        self.request = request
		self.service = service
        self.response = response
		self.httpResponse = httpResponse
		self.error = error
        if let responseCode = httpResponseCode {
            self.httpResponseCode = HTTPStatusCode(rawValue: responseCode)
        } else {
            self.httpResponseCode = nil
        }
	}
}

enum APIMethod: String {
    case get = "GET"
    case post = "POST"
    case head = "HEAD"
}
