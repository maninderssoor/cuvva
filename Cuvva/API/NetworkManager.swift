import Foundation

protocol Networking {
    func fetch<E: APIProtocol, P: APIProtocolParameters, R: APIProtocolResponse>(with type: E.Type, and parameters: P, and response: R.Type, completion: @escaping ((APIProtocolCompletion) -> Void))
}

class NetworkManager: Networking {
	
	// A network sessions
	let networkSession: URLSession

	/**
		Initalise with a configuration
	*/
    init() {
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
		self.networkSession = URLSession(configuration: sessionConfiguration, delegate: nil, delegateQueue: nil)
	}

    func fetch<E: APIProtocol, P: APIProtocolParameters, R: APIProtocolResponse>(with type: E.Type, and parameters: P, and response: R.Type, completion: @escaping ((APIProtocolCompletion) -> Void)) {

        guard let url = URL(string: "\(Constants.baseURL.rawValue)\(type.slug)") else {
			print("Need a valid URL")
			let completionResponse = APIProtocolCompletion(isSuccess: false)
			completion(completionResponse)
			return
		}
		
        var request = URLRequest(url: url)
        request.httpMethod = E.method.rawValue
        
        var headers = request.allHTTPHeaderFields ?? [:]
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        request.allHTTPHeaderFields = headers

        if E.method != .get {
            request.httpBody = parameters.parameters()
        }
        
		let dataTask = networkSession.dataTask(with: request) { (data, httpURLResponse, error) in

            var httpResponse: HTTPURLResponse?
            var httpResponseCode: Int?
            if let httpURLResponse = httpURLResponse as? HTTPURLResponse {
                httpResponse = httpURLResponse
                httpResponseCode = httpURLResponse.statusCode
            }
			var errorCode: Int?
			var errorMessage: String?
			if let urlSessionError = error as? NSError {
				errorCode = urlSessionError.code
				errorMessage = urlSessionError.description
			}

			guard let data = data, error == nil else {
				DispatchQueue.main.async {
                    let completionResponse = APIProtocolCompletion(isSuccess: false,
                                                                   request: request,
																   service: type,
                                                                   httpResponse: httpResponse,
                                                                   httpResponseCode: httpResponseCode,
                                                                   error: error,
																   urlErrorCode: errorCode,
																   urlErrorDescription: errorMessage)
					completion(completionResponse)
				}
				return
			}
            
            /// Check the response is  a success
            guard let responseCode = httpResponseCode, responseCode >= HTTPStatusCode.ok.rawValue && responseCode < 300 else {
                print("[NetworkManager] The Networking call wasn't successful. Returned HTTP response code \(String(describing: httpResponseCode))")
                DispatchQueue.main.async {
                    let completionResponse = APIProtocolCompletion(isSuccess: false,
                                                                   request: request,
																   service: type,
                                                                   httpResponse: httpResponse,
                                                                   httpResponseCode: httpResponseCode,
                                                                   error: error,
																   urlErrorCode: errorCode,
																   urlErrorDescription: errorMessage)
					completion(completionResponse)
                }
                return
            }
            
			/// Convert to object
			do {

				if let json = try? JSONSerialization.jsonObject(with: data, options: []) {
                    print("[NetworkManager] Successfully fetched data from URL \(String(describing: request.url?.absoluteString)) for \(String(describing: json))")
				}

                let decodedResponse = try JSONDecoder().decode(response, from: data)
                
				DispatchQueue.main.async {
                    let completionResponse = APIProtocolCompletion(isSuccess: true,
                                                                   request: request,
																   service: type,
                                                                   response: decodedResponse,
                                                                   httpResponse: httpResponse,
                                                                   httpResponseCode: httpResponseCode,
                                                                   error: error,
																   urlErrorCode: errorCode,
																   urlErrorDescription: errorMessage)
					completion(completionResponse)
				}
				
			} catch {
				print("[NetworkManager] Error in catch \(String(describing: error))")
				DispatchQueue.main.async {
                    let completionResponse = APIProtocolCompletion(isSuccess: false,
                                                                   request: request,
																   service: type,
                                                                   httpResponse: httpResponse,
                                                                   httpResponseCode: httpResponseCode,
                                                                   error: error,
																   urlErrorCode: errorCode,
																   urlErrorDescription: errorMessage)
					completion(completionResponse)
				}
				return
			}
			
        }

		dataTask.resume()
        
        print("[NetworkingManager] Request \(String(describing: dataTask.originalRequest?.debugDescription))")
	}
    
}
