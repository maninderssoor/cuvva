import Foundation

enum PolicyKeys: String {
    case entity = "Policy"
    
    case uuid
    
    case id
    case originalId
    case referenceCode
    
    case createdDateTime
    case endDateTime
    case startDateTime
    
    case telephone
    
    case extensionPolicies
    case transaction
    case vehicle
}
