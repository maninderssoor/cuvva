import Foundation

enum VehicleKeys: String {
    case entity = "Vehicle"

    case vrm
    case prettyVrm
    case make
    case model
    case color
    
    case policies
}
