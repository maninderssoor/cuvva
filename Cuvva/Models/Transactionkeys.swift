import Foundation

enum TransactionKeys: String {
    case entity = "Transaction"

    case uuid
    
    case createdDateTime
    case policyId
    
    case adminFee
    case premium
    case premiumTax
}
