# Cuvva

![Code Coverage](https://img.shields.io/badge/coverage-56%25-yellow.svg)
[![iOS Platform](https://img.shields.io/badge/platform-ios-lightgrey.svg)](https://img.shields.io/badge/platform-ios-lightgrey.svg)

## Task:

Retrieve the policy event stream from the end point : https://cuvva.herokuapp.com
 * Process the event data into a format that can be used to support the screen designs.
 * Implement a navigation from the home screen to the policy screen and then to the receipt screen.
 * The app only needs to work in portrait mode
 * The app needs to work on the following OS versions (relevant to the role applied for):
         * iOS : v11.0 to latest
        * Android : v5.0.1 to latest
 * Once the API data has been successfully retrieved the app should be able to continueworking even when not connected to the internet and after a forced close of the app. (e.g. data should be persisted)

## Notes

The Cuvva test app has been written with an MVVM architecture in mind, I tried to concentrate on foundational process like ensuring the necessary data is fetched and adapted for use in the app so it can be scaled and extended in the future.

UITableViews have been used for all views, Core Data used to persist data and view models used to communicate between data and controllers.

Where possible seperate storyboards and xib files were used. Basic UX/ UI concepts were templated for reusability.

Hopefully the foundations allow for easier scaling with the items that have been missed off.

Some testing has been implemented particularly on decoding the API response.

** Future Refinements **

Notable aspects I would like to refine where possible:

* The PolicyViewController doesn't present active policies, this is shown on the Home View Controller.
* There's an assumption that all policies are up to an hour, I don't think this is a real world example.
* Custom header views for the sections
* A placeholder when no active/ previous policies are available on the HomeViewController
* Time remaining to be more dynamic

## System Requirements

- iOS 11.0+
- Xcode 11.0+
- Swift 5.0

## Build

### Instructions

1. Checkout the application via Github or via a terminal.
2. Open the application in Xcode and run.

## Author

Maninder Soor (http://manindersoor.com)
