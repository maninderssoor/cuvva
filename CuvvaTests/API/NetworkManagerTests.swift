import XCTest
@testable import Cuvva

struct APINotFoundService: APIProtocol {
    static var title: String = "Image"
    static var slug: String = "https://www.cuvva.com/abo"
    static var method: APIMethod = .get
}

struct APIMovedURLParameters: APIProtocolParameters {
    let random = Int.random(in: 0...10)
}

private class NetworkManagerTests: CuvvaTests {

    private let networkManager = NetworkManager()
    
    func testNetworkingManager_givenBasicIntialisation_setupIsCorrect() {
        let session = networkManager.networkSession
        let configuration = session.configuration
        
        XCTAssertNotNil(session)
        XCTAssertEqual(configuration.requestCachePolicy, .reloadIgnoringLocalAndRemoteCacheData)
    }
    
    func testNetworkingManager_givenEmptyURL_setupsUpRequest() {
        let asynchronousExpectation = expectation(description: "testNetworkingManager_givenEmptyURL_setupsUpRequest")
        let emptyParameters = APITestParameters()
        networkManager.fetch(with: APITestNoURLService.self, and: emptyParameters, and: APITestResponse.self) { result in
            XCTAssertFalse(result.isSuccess)
            XCTAssertEqual(result.httpResponseCode, HTTPStatusCode.notFound)
            XCTAssertNil(result.error)

            asynchronousExpectation.fulfill()
            
        }
    
        waitForExpectations(timeout: timeout) { error in
            print("Expectations timed out with error \(String(describing: error))")
        }
    }

}
