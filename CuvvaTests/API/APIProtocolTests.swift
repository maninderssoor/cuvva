import XCTest
@testable import Cuvva


struct APITestNoURLService: APIProtocol {
    static var title: String = "NO URL"
    static var slug: String = "abo"
    static var method: APIMethod = .get
}

private class MockNetworkingNoURL {
    func fetch<E, P, R>(with type: E.Type, and parameters: P, and response: R.Type, completion: @escaping ((APIProtocolCompletion) -> Void)) where E : APIProtocol, P : APIProtocolParameters, R : APIProtocolResponse {
        completion(APIProtocolCompletion(isSuccess: true, service: type))
	}
}

struct APITestParameters: APIProtocolParameters {}

struct APITestResponse: APIProtocolResponse {}

struct TestURLEncoding: APIProtocolParameters {
    let title: String
    let gousto: Bool
}

private class NetworkingTests: CuvvaTests {

    func testNoURLConnectionsAPICall() {
        let mockNetworkingNoURL = MockNetworkingNoURL()
        let emptyParameters = APITestParameters()
        mockNetworkingNoURL.fetch(with: APITestNoURLService.self, and: emptyParameters, and: APITestResponse.self) { results in
            
            XCTAssertNotNil(results, "The results should not be nil")
        }
    }
}
