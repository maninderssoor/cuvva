import XCTest
@testable import Cuvva

class MockEmptyNetworkTests: CuvvaTests {

    func testMockEmptyNetwork_call_returns() {
        let network = MockNetworkEmptyResponse()

        network.fetch(with: APITestNoURLService.self, and: APITestParameters(), and: APITestResponse.self) { result in
            XCTAssertFalse(result.isSuccess)
        }
    }

}
