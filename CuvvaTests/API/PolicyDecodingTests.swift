import XCTest
@testable import Cuvva

class PolicyDecodingTests: CuvvaTests {

	func testPolicyDecoding_withJSON_isValue() {
		guard let jsonString = mockPolicyJSON.data(using: .utf8) else {
			XCTFail("Couldn't convert the JSON string into data")
			return
		}

		do {
			let result = try JSONDecoder().decode(APIPolicyResponse.self, from: jsonString)

            XCTAssertEqual(result.policies.count, 66)
            
            let created = result.policies.compactMap({ $0.policyPayload })
            let transactions = result.policies.compactMap({ $0.transactionPayload })
            
            XCTAssertEqual(created.count, 32)
            XCTAssertEqual(transactions.count, 33)
		} catch {
			XCTFail("Couldn't decode the json string into a State Report \(String(describing: error))")
		}
	}
}
