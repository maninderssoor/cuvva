import Foundation
@testable import Cuvva

let mockPolicyPayload = APIPolicyPayload(user_id: "user_000000BSJ47k7mKYfWUhkWOrxLYGm",
                                         policy_id: "dev_pol_000000BansDm7Jjbj3k4R1IUJwrEe",
                                         original_policy_id: "dev_pol_000000BansDm7Jjbj3k4R1IUJwrEe",
                                         reference_code: "PP32DFCS5W",
                                         start_date: "2019-01-18T10:15:30.000Z",
                                         end_date: "2019-04-18T11:15:30.000Z",
                                         incident_phone: "+442038287127",
                                         vrm: "LB07SEO",
                                         prettyVrm: "LB07 SEO",
                                         make: "Volkswagen",
                                         model: "Polo",
                                         color: "Silver")

let mockTransactionPayload = APITransactionPayload(policy_id: "dev_pol_000000BansDm7Jjbj3k4R1IUJwrEe",
                                                   total_premium: 665,
                                                   ipt: 80,
                                                   extra_fees: 125,
                                                   total_payable: 870)

let mockPolicy = APIPolicy(uuid: NSUUID().uuidString,
                           timestamp: "2019-01-18T10:15:29.979Z",
                           policyPayload: mockPolicyPayload,
                           transactionPayload: nil)

let mockPolicyTransaction = APIPolicy(uuid: NSUUID().uuidString,
                           timestamp: "2019-01-18T10:15:32.250Z",
                           policyPayload: nil,
                           transactionPayload: mockTransactionPayload)
