import Foundation
import CoreData
import XCTest
@testable import Cuvva

class MockCoreData: Database {
    func setup() {}

    var objects = [NSManagedObject]()

    lazy var managedObjectModel: NSManagedObjectModel = {
        let managedObjectModel = NSManagedObjectModel.mergedModel(from: [Bundle(for: type(of: self))] )!
        return managedObjectModel
    }()

    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "CoreData", managedObjectModel: self.managedObjectModel)
        let description = NSPersistentStoreDescription()
        description.type = NSInMemoryStoreType
        description.shouldAddStoreAsynchronously = false

        container.persistentStoreDescriptions = [description]
        container.loadPersistentStores { (description, error) in
            // Check if the data store is in memory
            precondition( description.type == NSInMemoryStoreType )

            // Check if creating container wrong
            if let error = error {
                fatalError("Create an in-memory coordinator failed \(error)")
            }
        }
        return container
    }()
    
    var privateContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)

    func save() {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

    func fetch(for table: String, filter predicate: NSPredicate?, sorting sort: [NSSortDescriptor]?, with context: NSManagedObjectContext?) -> [NSManagedObject]? {
        var finalContext = persistentContainer.viewContext
        if let context = context {
            finalContext = context
        }
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: table)

        if let predicate = predicate {
            request.predicate = predicate
        }

        if let sorting = sort {
            request.sortDescriptors = sorting
        }

        do {
            let fetched = try finalContext.fetch(request)

            print("[MockCoreData] Fetched \(table): \(fetched.count) from CoreData.")
            return fetched as? [NSManagedObject]
        } catch {
            print("[MockCoreData] Could not fetch \(table) from CoreData.")
            return nil
        }
    }

    func delete(in table: String) {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: table)
        do {
            guard let objects = try persistentContainer.viewContext.fetch(request) as? [NSManagedObject] else { return }

            for object in objects {
                persistentContainer.viewContext.delete(object)
            }

            save()
            print("[MockCoreData] Deleted \(objects.count) items in \(table)")
        } catch {
            print("[MockCoreData] Couldn't delete items from \(table)")
            return
        }
    }

    @discardableResult
    func save(in table: String, with parameters: [String: Any], overwritePredicate overrite: Bool, overwritePredicate predicate: NSPredicate?) -> NSManagedObject? {
        guard let entityDescription = NSEntityDescription.entity(forEntityName: table, in: persistentContainer.viewContext) else {
            print("[MockCoreData] Couldn't initialise an entity description for insertion given entity name \(table)")
            return nil
        }

        var object: NSManagedObject?

        if let predicate = predicate,
            let exisitingObject = fetch(for: table, filter: predicate, sorting: nil, with: persistentContainer.viewContext)?.first, overrite {
            object = exisitingObject
        } else {
            object = NSManagedObject(entity: entityDescription, insertInto: persistentContainer.viewContext)
        }

        guard let newObject = object else {
            print("[MockCoreData] Couldn't unwrap a new/ exisiting object")
            return nil
        }

        // Set the parameteers
        for parameter in parameters {
            newObject.setValue(parameter.value, forKey: parameter.key)
        }

        do {
            try persistentContainer.viewContext.save()
            return newObject
        } catch {
            let nserror = error as NSError
            print("Unresolved error \(nserror), \(nserror.userInfo)")
            return nil
        }
    }
}
