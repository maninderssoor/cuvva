//
//  CuvvaTests.swift
//  CuvvaTests
//
//  Created by Maninder Soor on 05/08/2020.
//  Copyright © 2020 Maninder. All rights reserved.
//

import XCTest
@testable import Cuvva

class CuvvaTests: XCTestCase {
    
    let timeout: TimeInterval = 6.0
    let databaseManager = CoreDataManager()
    let mockDatabaseManager = MockCoreData()
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        
        databaseManager.setup()
    }
    
    func clearDatabase() {
        databaseManager.delete(in: PolicyKeys.entity.rawValue)
        databaseManager.delete(in: TransactionKeys.entity.rawValue)
        databaseManager.delete(in: VehicleKeys.entity.rawValue)
    }
}
