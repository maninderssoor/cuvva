import XCTest
@testable import Cuvva

class PolicyAdapterTests: CuvvaTests {
    
    func testPolicyAdapter_givenPolicy_returnsPolicy() {
        let adapter = APIPolicyAdapter()
        guard let policy = adapter.toPolicy(with: mockPolicy) else {
            XCTFail()
            return
        }
        
        XCTAssertEqual(policy[PolicyKeys.uuid.rawValue] as? String, mockPolicy.uuid)
        XCTAssertEqual(policy[PolicyKeys.id.rawValue] as? String, mockPolicyPayload.policy_id)
        XCTAssertEqual(policy[PolicyKeys.originalId.rawValue] as? String, mockPolicyPayload.original_policy_id)
        
        guard let createdDate = policy[PolicyKeys.createdDateTime.rawValue] as? Date,
            let startDate = policy[PolicyKeys.startDateTime.rawValue] as? Date,
            let endDate = policy[PolicyKeys.endDateTime.rawValue] as? Date else {
                XCTFail()
                return
        }
        let createdDateComponents = Calendar.current.dateComponents([.day, .year, .month, .hour, .minute, .second], from: createdDate)
        
        XCTAssertEqual(createdDateComponents.year, 2019)
        XCTAssertEqual(createdDateComponents.month, 01)
        XCTAssertEqual(createdDateComponents.day, 18)
        XCTAssertEqual(createdDateComponents.hour, 10)
        XCTAssertEqual(createdDateComponents.minute, 15)
        XCTAssertEqual(createdDateComponents.second, 29)
        
        
        let startDateComponents = Calendar.current.dateComponents([.day, .year, .month, .hour, .minute, .second], from: startDate)
        
        XCTAssertEqual(startDateComponents.year, 2019)
        XCTAssertEqual(startDateComponents.month, 01)
        XCTAssertEqual(startDateComponents.day, 18)
        XCTAssertEqual(startDateComponents.hour, 10)
        XCTAssertEqual(startDateComponents.minute, 15)
        XCTAssertEqual(startDateComponents.second, 30)
        
        let endDateComponents = Calendar.current.dateComponents([.day, .year, .month, .hour, .minute, .second], from: endDate)
        
        XCTAssertEqual(endDateComponents.year, 2019)
        XCTAssertEqual(endDateComponents.month, 4)
        XCTAssertEqual(endDateComponents.day, 18)
        XCTAssertEqual(endDateComponents.hour, 12)
        XCTAssertEqual(endDateComponents.minute, 15)
        XCTAssertEqual(endDateComponents.second, 30)
        
        
        XCTAssertEqual(policy[PolicyKeys.telephone.rawValue] as? String, mockPolicyPayload.incident_phone)
        XCTAssertEqual(policy[PolicyKeys.referenceCode.rawValue] as? String, mockPolicyPayload.reference_code)
    }
    
    func testPolicyAdapter_givenPolicy_returnsVehicle() {
        let adapter = APIPolicyAdapter()
        guard let vehicle = adapter.toVehicle(with: mockPolicy) else {
            XCTFail()
            return
        }
        
        XCTAssertEqual(vehicle[VehicleKeys.vrm.rawValue] as? String, mockPolicyPayload.vrm)
        XCTAssertEqual(vehicle[VehicleKeys.prettyVrm.rawValue] as? String, mockPolicyPayload.prettyVrm)
        XCTAssertEqual(vehicle[VehicleKeys.make.rawValue] as? String, mockPolicyPayload.make)
        XCTAssertEqual(vehicle[VehicleKeys.model.rawValue] as? String, mockPolicyPayload.model)
        XCTAssertEqual(vehicle[VehicleKeys.color.rawValue] as? String, mockPolicyPayload.color)
    }
    
    func testPolicyAdapter_givenPolicy_returnsTransaction() {
        let adapter = APIPolicyAdapter()
        guard let transaction = adapter.toTransaction(with: mockPolicyTransaction) else {
            XCTFail()
            return
        }
        
        XCTAssertEqual(transaction[TransactionKeys.adminFee.rawValue] as? Decimal, Decimal(mockTransactionPayload.extra_fees))
        XCTAssertEqual(transaction[TransactionKeys.policyId.rawValue] as? String, mockTransactionPayload.policy_id)
        XCTAssertEqual(transaction[TransactionKeys.uuid.rawValue] as? String, mockPolicyTransaction.uuid)
        XCTAssertEqual(transaction[TransactionKeys.premium.rawValue] as? Decimal, Decimal(mockTransactionPayload.total_premium))
        XCTAssertEqual(transaction[TransactionKeys.premiumTax.rawValue] as? Decimal, Decimal(mockTransactionPayload.ipt))
        
        guard let createdDate = transaction[TransactionKeys.createdDateTime.rawValue] as? Date else {
            XCTFail()
            return
        }
        let createdDateComponents = Calendar.current.dateComponents([.day, .year, .month, .hour, .minute, .second], from: createdDate)
        
        XCTAssertEqual(createdDateComponents.year, 2019)
        XCTAssertEqual(createdDateComponents.month, 01)
        XCTAssertEqual(createdDateComponents.day, 18)
        XCTAssertEqual(createdDateComponents.hour, 10)
        XCTAssertEqual(createdDateComponents.minute, 15)
        XCTAssertEqual(createdDateComponents.second, 32)
    }
}
